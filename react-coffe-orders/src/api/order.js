import { createHeders } from "."

const apiURL = process.env.REACT_APP_API_URL

export const orderAdd = async (user, order) => {

    try{
        const response = await fetch(`${apiURL}/${user.id}`,{
            method: 'PATCH',
            headers:createHeders(),
            body:JSON.stringify({
                order:[...user.order, order]
            })
        })
        if(!response.ok){
            throw new Error('could not update the order')
        }

        const result = await response.json()
        return[null,result]

    }catch(error)
    {
        return[error.message, null]

    }


}

export const orderClearHistory = (userId) => {

}