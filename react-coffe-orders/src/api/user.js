
import { createHeders } from './index'

const apiURL = process.env.REACT_APP_API_URL

const cheakForUser = async (username) => {
    try {
        const response = await fetch(`${apiURL}?username=${username}`)
        if (!response.ok) {
            throw new Error("could not complite request")
        }
        const data = await response.json()
        return [null, data]
    }
    catch (error) {
        return [error.message, []]

    }
}
const createUser = async (username) => {

    try {
        const response = await fetch(apiURL, {
            method: 'POST', // create a resorse
            headers: createHeders(),
            body: JSON.stringify({
                username,
                order: []
            })
        })
        if (!response.ok) {
            throw new Error("could not create user " + username)
        }
        const data = await response.json()
        return [null, data]
    }
    catch (error) {
        return [error.message, []]

    }
}
export const loginUser = async (username) => {
    const [cheakError, user] = await cheakForUser(username)

    if (cheakError !== null) {
        return [cheakError, null]

    }

    if (user.length > 0) {
        // user does not exist
        return [null, user.pop()]
    }

    return await createUser(username)



}



export const userById = async (userId) => {
    try {
        const response = await fetch(`${apiURL}/${userId}`)
        if (!response.ok) {
            throw new Error('cold not fetch user')

        }
        const user = await response.json()
        return[null,user]
    }
    catch (error) {
        return [error.message,null]

    }
}
