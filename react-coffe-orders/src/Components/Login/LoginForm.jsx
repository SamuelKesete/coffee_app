
import { useState, useEffect } from "react";
import { useForm } from "react-hook-form"
import { loginUser } from '../../api/user'
import { storageSave } from "../../utils/storege";
import { useNavigate } from 'react-router-dom'
import { useUser } from "../../context/UserContext";
import { STORAGE_KEY_USER } from "../../const/storageKeys";
//import '../App.css';

const usernameConfig = {
    required: true,
    minLength: 2,
};
const LoginForm = () => {
    const { register, handleSubmit, formState: { errors } } = useForm();
    const  {user,setUser} = useUser()
    const navigate = useNavigate()

    //Local state
    const [loading, setLoading] = useState(false)
    const [apiError, setApiError] = useState(null)

//Side effect

useEffect( ()=>{
    if(user !== null){
        navigate('profile')
    }

  console.log('User has change!', user)

},[user, navigate])// empty deps -only run 1ce
// Event handlr
    const onSubmit = async ({ username }) => {
        setLoading(true)
        const [error, userResponse] = await loginUser(username)
        if (error !== null) {
            setApiError(error)
        }
        if (userResponse !== null) {
            storageSave(STORAGE_KEY_USER, userResponse)
            setUser(userResponse)

        }
        setLoading(false)

    };
// render function
    const errorMessage = (() => {

        if (!errors.username) {
            return null
        }
        if (errors.username.type === 'required') {
            return <span>username is required</span>

        }
        if (errors.username.type === 'minLength') {
            return <span>username is to short</span>

        }
    })()


    return (
        <>
            <h2>What is yor name?</h2>

            <form id="form" onSubmit={handleSubmit(onSubmit)}>
                <fieldset>
                    <label htmlFor="username"></label>
                    <input type="text"
                        placeholder="Name"
                        {...register("username", usernameConfig)}

                    />
                    {errorMessage}
                </fieldset>
                <button type="submit" disabled={loading}>Submit</button>
                {loading && <p>Logging in...</p>}
                {apiError && <p>{apiError}</p>}
            </form>

        </>
    )

}
export default LoginForm;