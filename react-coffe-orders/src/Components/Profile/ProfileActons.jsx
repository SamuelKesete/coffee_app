
import { Link } from "react-router-dom"
import { STORAGE_KEY_USER } from "../../const/storageKeys"
import { useUser } from "../../context/UserContext"
import { storageDelete } from "../../utils/storege"


const ProfileAction = ({ logout }) => {

    const { setUser } = useUser()

    const handelLogOutClick = () => {
        if (window.confirm('Are you sure?')) {
            storageDelete(STORAGE_KEY_USER)
            setUser(null)
        }
    }

    return (
        <ul>
            <li> <Link to="/orders">Orders</Link></li>
            <li><button>Clear history</button></li>
            <li> <button onClick={handelLogOutClick}>Logout</button></li>
        </ul>
    )
}
export default ProfileAction