
import { Children, createContext, useContext, useState } from "react";
import { STORAGE_KEY_USER } from "../const/storageKeys";
import { storageRead } from "../utils/storege";
// context object



const UserContext = createContext()
export const useUser = () => {
    return useContext(UserContext)
}

// provider -> managing state 

const UserProvider = ({ children }) => {
    // magic strings 
    const [user, setUser] = useState(storageRead(STORAGE_KEY_USER))

    const state = {
        user,
        setUser
    }
    return (
        <UserContext.Provider value={state}>
            {children}
        </UserContext.Provider>
    )
}
export default UserProvider