import { useEffect } from "react"
import { userById } from "../api/user"
import ProfileAction from "../Components/Profile/ProfileActons"
import ProfileHeder from "../Components/Profile/ProfileHeder"
import ProfileOrderHistory from "../Components/Profile/ProfileOrderHistory"
import { STORAGE_KEY_USER } from "../const/storageKeys"
import { useUser } from "../context/UserContext"
import withAuth from "../hoc/withAuth"
import { storageSave } from "../utils/storege"


const Profile = () => {

    const { user, setUser } = useUser()

    useEffect(()=>{
        const findUser = async()=>{
            const[error,latestUser] = await userById(user.id)
            if(error === null){
                storageSave(STORAGE_KEY_USER, latestUser)
                setUser(latestUser)
            }
        }
       // findUser()
    },[setUser, user.id])
    
    return (
        <>
            <h1>Profile</h1>
            <ProfileHeder username={user.username} />
            <ProfileAction />
            <ProfileOrderHistory orders={user.orders} />
        </>
    )
}
export default withAuth(Profile)