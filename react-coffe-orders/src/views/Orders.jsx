import { useState } from "react"
import { orderAdd } from "../api/order"
import OrdersCoffeeButtons from "../Components/Orders/OrdersCoffeeButtons"
import OrdersForm from "../Components/Orders/OrdersForm"
import { STORAGE_KEY_USER } from "../const/storageKeys"
import { useUser } from "../context/UserContext"
import withAuth from "../hoc/withAuth"
import { storageSave } from "../utils/storege"

const COFFEES = [
    {
        id: 1,
        name: 'Americano',
        image: "img/americano.png"
    },
    {
        id: 2,
        name: 'Cappuccino',
        image: "img/cappuccino.png"
    },
    {
        id: 3,
        name: 'Latte',
        image: "img/latte.png"
    },
    {
        id: 4,
        name: 'Espresso',
        image: "img/espresso.png"
    }
]


const Orders = () => {


const [coffee, setCoffee] = useState(null)
const{user, setUser} = useUser()



    const handleCoffeeCklick = (coffeeId) => {
       // console.log(coffeeId)
       setCoffee(COFFEES.find(coffee => coffee.id ===coffeeId))

    }
    const handeleOrderClick = async notes =>{
        console.log(notes)
        
        if(!coffee){
            alert("Please select a coffee first")
            return
        }

        const order = (coffee.name + ' ' + notes).trim() 
        
        const [error, uppdatatedUser] = await orderAdd(user, order)
        if(error !==null){
            return
        }
        storageSave(STORAGE_KEY_USER, uppdatatedUser)
        setUser(uppdatatedUser)

        console.log('error', error)
        console.log('uppdatateUser', uppdatatedUser)
    }

    const avalibleCoffees = COFFEES.map(coffee => {
        return <OrdersCoffeeButtons
            key={coffee.id}
            coffee={coffee}
            onSelect={handleCoffeeCklick} />

    })

    return (
        <>
            <h1>Orders</h1>
            <section id="coffee-option">
                {avalibleCoffees}
            </section>
            <section id="order-notes">
                <OrdersForm onOrder={handeleOrderClick}/>

            </section>
            <h4> Summery</h4>
            {coffee && <p>Selected coffee: {coffee.name}</p>}
        </>
    )
}
export default withAuth(Orders)